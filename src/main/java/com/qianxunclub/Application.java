package com.qianxunclub;

import com.qianxunclub.starter.swagger.autoconfigure.EnableSBCSwagger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @author chihiro.zhang
 */
@Slf4j
@EnableSBCSwagger
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		log.info("日志信息");
	}

}