package com.qianxunclub.framework.generate.service;

import com.alibaba.druid.filter.config.ConfigTools;
import com.qianxunclub.common.framework.response.Result;
import com.qianxunclub.common.tools.utils.JsonUtil;
import com.qianxunclub.framework.generate.configuration.Constant;
import com.qianxunclub.framework.generate.model.TableInfo;
import com.qianxunclub.framework.generate.model.TemplateParam;
import lombok.AllArgsConstructor;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * @author chihiro.zhang
 */
@Component
@AllArgsConstructor
public class GenerateService {

    private VelocityEngine velocityEngine;
    private Constant constant;
    private DruidDataSourceService druidDataSourceService;

    public Result generate(TemplateParam param){
        String rootDir = constant.getRootDir();
        rootDir = rootDir + "qianxunclub-" + param.getGroup() + "-" + param.getName() + "/";

        //清空项目目录
        File file = new File(rootDir);
        if(file.exists()){
            deleteDir(file);
        }

        //生成项目根目录以及根目录文件
        this.root(param,rootDir);
        //生成Application.java
        this.application(param,rootDir);
        //生成resources
        this.resources(param,rootDir);
        if(param.isMysqlSupport()){
            this.dataSource(param,rootDir);
        } else {
            TableInfo tableInfo = new TableInfo();
            tableInfo.setName("Demo");
            tableInfo.setLowerName("demo");
            tableInfo.setRemarks("Demo示例");
            param.setTableInfo(tableInfo);
            this.demoDataSource(param,rootDir);
        }
        return Result.ok().setResult(rootDir);
    }

    private boolean root(TemplateParam param, String rootDir){
        //生成pom.xml
        String content = this.context("pom.vm",getVelocityContext(param));
        this.createFile(rootDir + "pom.xml",content);
        //生成.gitignore
        content = this.context("gitignore.vm",null);
        this.createFile(rootDir + ".gitignore",content);
        return true;
    }

    private boolean application(TemplateParam param, String rootDir){

        String content = this.context("application.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/java/com/qianxunclub/Application.java",content);

        return true;
    }

    private boolean resources(TemplateParam param, String rootDir){
        if(param.isMysqlSupport()){
            try {
                String[] arr = ConfigTools.genKeyPair(512);
                param.getDatasource().setPublicKey(arr[1]);
                param.getDatasource().setPass(ConfigTools.encrypt(arr[0], param.getDatasource().getPassword()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String content = this.context("resources/application.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/resources/application.yml",content);
        content = this.context("resources/application-dev.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/resources/application-dev.yml",content);
        content = this.context("resources/application-prod.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/resources/application-prod.yml",content);

        return true;
    }

    private boolean dataSource(TemplateParam param, String rootDir){
        //读取数据库表
        List<TableInfo> tableInfoList = druidDataSourceService.getTabls(param.getDatasource());
        tableInfoList.forEach(tableInfo -> {
            param.setTableInfo(tableInfo);

            String content = this.context("entity.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/storage/entity/" + tableInfo.getName() + "Entity.java",content);

            content = this.context("param.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/model/request/" + tableInfo.getName() + "Param.java",content);

            content = this.context("mapper.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/storage/mapper/" + tableInfo.getName() + "Mapper.java",content);

            content = this.context("dao.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/storage/dao/" + tableInfo.getName() + "Dao.java",content);

            content = this.context("service.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/service/" + tableInfo.getName() + "Service.java",content);

            content = this.context("web.vm",getVelocityContext(param));
            this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/web/" + tableInfo.getName() + "Controller.java",content);

        });

        return true;
    }

    private boolean demoDataSource(TemplateParam param, String rootDir){
        TableInfo tableInfo = param.getTableInfo();
        String content = this.context("param.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/model/request/" + tableInfo.getName() + "Param.java",content);

        content = this.context("service.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/service/" + tableInfo.getName() + "Service.java",content);

        content = this.context("web.vm",getVelocityContext(param));
        this.createFile(rootDir + "src/main/java/com/qianxunclub/" + param.getGroup() + "/" + param.getName() + "/web/" + tableInfo.getName() + "Controller.java",content);
        return true;
    }


    private void createFile(String filePath, String filecontent){
        BufferedWriter bw = null;
        File file = new File(filePath);
        try {

            if(!file.exists()){
                file.getParentFile().mkdirs();
            } else {
                file.delete();
            }
            file.createNewFile();
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            bw.write(filecontent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bw.flush();
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }
    private String velocityEngine(String templates,VelocityContext ctx){
        Template t = velocityEngine.getTemplate("templates/" + templates);
        StringWriter sw = new StringWriter();
        t.merge(ctx,sw);
        return sw.toString();
    }

    private VelocityContext getVelocityContext(Object param){
        VelocityContext ctx = new VelocityContext();
        Map<String,Object> map = JsonUtil.jsonToMap(JsonUtil.objectToJson(param));
        map.keySet().forEach(k->
                ctx.put(k,map.get(k))
        );
        return ctx;
    }

    private String context(String templatesPath,VelocityContext ctx){
        return this.velocityEngine(templatesPath,ctx);
    }
}
