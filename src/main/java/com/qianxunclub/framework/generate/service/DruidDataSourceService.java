package com.qianxunclub.framework.generate.service;


import com.alibaba.druid.pool.DruidDataSource;
import com.google.common.base.CaseFormat;
import com.qianxunclub.framework.generate.model.CloumnInfo;
import com.qianxunclub.framework.generate.model.DatasourceParam;
import com.qianxunclub.framework.generate.model.TableInfo;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chihiro.zhang
 */
@Component
public class DruidDataSourceService {

    public List<TableInfo> getTabls(DatasourceParam param){
        List<TableInfo> tableInfoList = new ArrayList<>();
        Connection connection = null;
        try {
            connection = this.dataSource(param).getConnection();
            DatabaseMetaData dbMetaData = connection.getMetaData();
            ResultSet rs = dbMetaData.getTables(null, null, null,new String[] { "TABLE" });
            while (rs.next()) {
                TableInfo tableInfo = new TableInfo();
                tableInfo.setTableName(rs.getString("TABLE_NAME"));
                String fildeName = tableInfo.getTableName();
                if(fildeName.indexOf("t_") == 0){
                    fildeName = fildeName.substring(2);
                }
                tableInfo.setName(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, fildeName));
                tableInfo.setLowerName(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, fildeName));
                tableInfo.setRemarks(rs.getString("REMARKS"));
                String sql = "select * from " + tableInfo.getTableName();
                try {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ResultSet tableRs = ps.executeQuery();
                    ResultSetMetaData meta = tableRs.getMetaData();
                    int columeCount = meta.getColumnCount();
                    for (int i = 1; i < columeCount + 1; i++) {
                        String filedName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, meta.getColumnName(i));
                        if(filedName .contentEquals("enabled|deleted|createUser|createIp|createDate|updateUser|updateIp|updateDate|versionNo")){
                            continue;
                        }
                        CloumnInfo cloumnInfo = new CloumnInfo();
                        cloumnInfo.setName(filedName);
                        cloumnInfo.setDataType(getDataType(meta.getColumnType(i)));
                        tableInfo.getCloumnInfoList().add(cloumnInfo);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                tableInfoList.add(tableInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return tableInfoList;
    }

    private DruidDataSource dataSource(DatasourceParam param) {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://" + param.getHost() + ":" + param.getPort() + "/" + param.getDatabase() + "?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true");
        dataSource.setUsername(param.getUsername());
        dataSource.setPassword(param.getPassword());
        DatabaseDriver databaseDriver = DatabaseDriver.fromJdbcUrl("jdbc:mysql://" + param.getHost() + ":" + param.getPort() + "/" + param.getDatabase() + "?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true");
        String validationQuery = databaseDriver.getValidationQuery();
        if (validationQuery != null) {
            dataSource.setTestOnBorrow(true);
            dataSource.setValidationQuery(validationQuery);
        }
        return dataSource;
    }

    private String getDataType(int dataType){
        switch (dataType){
            case -6:
            case  5:
            case 4:
            case -5: return "Integer";
            case 6:
            case 7:
            case 8:
            case 2:
            case 3: return "Double";
            case 91:
            case 92:
            case 93: return "Date";
            default: return "String";
        }
    }

}
