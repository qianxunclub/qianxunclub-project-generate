package com.qianxunclub.framework.generate.configuration;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chihiro.zhang
 */
@Configuration
public class VelocityConfiguration {

    @Bean
    public VelocityEngine velocityEngine(){
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.setProperty(Velocity.ENCODING_DEFAULT,"utf-8");
        velocityEngine.setProperty(Velocity.INPUT_ENCODING,"utf-8");
        velocityEngine.setProperty(Velocity.OUTPUT_ENCODING,"utf-8");
        velocityEngine.init();
        return velocityEngine;
    }
}
