package com.qianxunclub.framework.generate.configuration;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author chihiro.zhang
 */
@Component
@Data
@ConfigurationProperties(prefix="constant")
public class Constant {

    private String rootDir;
    private String rootPackage;

}
