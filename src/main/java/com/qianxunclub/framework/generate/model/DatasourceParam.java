package com.qianxunclub.framework.generate.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author chihiro.zhang
 */
@Data
public class DatasourceParam {
    @NotNull
    @ApiModelProperty(value="数据库host")
    private String host;
    @NotNull
    @ApiModelProperty(value="数据库port")
    private String port;
    @NotNull
    @ApiModelProperty(value="数据库database")
    private String database;
    @NotNull
    @ApiModelProperty(value="数据库用户名")
    private String username;
    @NotNull
    @ApiModelProperty(value="数据库明文密码")
    private String password;
    private String pass;
    private String publicKey;
}
