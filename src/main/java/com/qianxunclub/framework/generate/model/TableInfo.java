package com.qianxunclub.framework.generate.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chihiro.zhang
 */
@Data
public class TableInfo {
    private String name;
    private String lowerName;
    private String tableName;
    private String remarks;
    private List<CloumnInfo> cloumnInfoList = new ArrayList<>();
}
