package com.qianxunclub.framework.generate.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author chihiro.zhang
 */
@Data
public class TemplateParam {
    @NotNull
    @ApiModelProperty(value="项目分组名称",example = "framework")
    private String group;
    @NotNull
    @ApiModelProperty(value="项目名称",example = "demo")
    private String name;
    @NotNull
    @ApiModelProperty(value="描述")
    private String description;
    @NotNull
    @ApiModelProperty(value="负责人")
    private String author;
    @NotNull
    @ApiModelProperty(value="负责人邮箱")
    private String email;
    @ApiModelProperty(value="qianxunclub-starter-parent版本,默认为0.0.1-SNAPSHOT",example = "0.0.1-SNAPSHOT")
    private String parentVersion = "1.0.2-SNAPSHOT";
    @ApiModelProperty(value="是否使用MYSQL，默认不使用")
    private boolean mysqlSupport = false;
    @ApiModelProperty(value="是否使用REDIS，默认不使用")
    private boolean redisSupport = false;
    @ApiModelProperty(value="Entity是否继承BaseEntity，默认继承")
    private boolean extendsBaseEntity = true;
    @ApiModelProperty(value="数据源配置")
    private DatasourceParam datasource;
    private TableInfo tableInfo;
}
