package com.qianxunclub.framework.generate.model;

import lombok.Data;

/**
 * @author chihiro.zhang
 */
@Data
public class CloumnInfo {
    private String name;
    private String dataType;
    private Boolean haveDate;

}
