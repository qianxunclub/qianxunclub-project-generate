package com.qianxunclub.framework.generate.web;

import com.qianxunclub.common.framework.constant.CommonReturnCode;
import com.qianxunclub.common.framework.response.Result;
import com.qianxunclub.framework.generate.model.DatasourceParam;
import com.qianxunclub.framework.generate.model.TemplateParam;
import com.qianxunclub.framework.generate.service.GenerateService;
import com.qianxunclub.framework.generate.service.ZipUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Validator;
import java.io.*;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Set;


/**
 * @author chihiro.zhang
 */
@Slf4j
@RestController
@RequestMapping(value = "/")
@Api(description = "代码生成")
@AllArgsConstructor
public class GenerateController {

	private GenerateService generateService;
	private Validator validator;

	@ApiOperation("generate")
	@ResponseBody
	@RequestMapping(value="generate",method = RequestMethod.POST)
	public Result generate(@RequestBody TemplateParam param) {
		Result result = Result.ok();
		if(param.isMysqlSupport()){
			DatasourceParam datasourceParam = param.getDatasource();
			Set<ConstraintViolation<Object>> constraintViolations = this.validator.validate(datasourceParam, new Class[0]);
			if (constraintViolations.size() > 0) {
				Iterator iterator = constraintViolations.iterator();
				if (iterator.hasNext()) {
					ConstraintViolation<Object> constraintViolation = (ConstraintViolation)iterator.next();
					Path property = constraintViolation.getPropertyPath();
					String name = property.toString();
					result = Result.code(CommonReturnCode.PARAM_ERROR).setMessage("[" + name + "]" + constraintViolation.getMessage());
				}
				log.error(result.getMessage());
				log.debug("→→→→→generate>>>>Return to the result :" + result.getMessage());
				return result;
			}
		}
		result = generateService.generate(param);
		String prijectDir = (String) result.getResult();
		File file = ZipUtil.zip(prijectDir);
		try {
			ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			HttpServletResponse response = requestAttributes.getResponse();
			response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode(file.getName(),"UTF-8"));
			FileInputStream fis =  new FileInputStream(file);
			ServletOutputStream out = response.getOutputStream();
			byte buffer[] = new byte[1024];
			//定义读取长度
			int len = 1024;
			//循环读取
			while((len = fis.read(buffer))!=-1){
				out.write(buffer,0,len);
			}
			//释放资源
			fis.close();
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//清空项目目录
		File rootFile = new File(prijectDir);
		if(rootFile.exists()){
			GenerateService.deleteDir(rootFile);
		}
		file.delete();
		return Result.ok();
	}

}
