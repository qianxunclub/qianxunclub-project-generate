1. 项目启动成功，访问：localhost:8080/swagger-ui.html  
2. 填写入参：
```json
{
  "group": "baseplatform",
  "name": "demo",
  "author": "项目负责人",
  "description": "项目描述",
  "email": "960339491@qq.com",
  "parentVersion": "1.0.2-SNAPSHOT",
  "extendsBaseEntity": true,
  "redisSupport": false,
  "mysqlSupport": true,
  "datasource": {
    "database": "baseplatform_oneway",
    "host": "192.168.35.228",
    "username": "tim",
    "password": "tim",
    "port": "4310"
  }
}
```

